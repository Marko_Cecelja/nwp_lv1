<?php
include("iRadovi.php");
include("dbConfig.php");

//klasa koja implementira sučelje iRadovi te pruža implementaciju potrebnih metoda
class DiplomskiRadovi implements iRadovi
{

    //parametri klase
    private $conn;

    private $id;

    private $naziv_rada;

    private $tekst_rada;

    private $link_rada;

    private $oib_tvrtke;

    //konstruktor bez argumenata koji inicijalizira pdo objekt
    function __construct()
    {

        $connStr = sprintf("mysql:host=%s;dbname=%s", DBConfig::HOST, DBConfig::DB_NAME);

        try {
            $this->conn = new PDO($connStr, DBConfig::USERNAME, DBConfig::PASS);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    //destructor koji postavlja pdo objekt na null
    public function __destruct()
    {
        $this->conn = null;
    }

    //implementacija create funkcije koja postavlja parametre klase
    function create($naziv_rada, $tekst_rada, $link_rada, $oib_tvtke)
    {
        $this->_naziv_rada = $naziv_rada;
        $this->_tekst_rada = $tekst_rada;
        $this->_link_rada = $link_rada;
        $this->_oib_tvrtke = $oib_tvtke;
    }

    //implementacija save metode koja pomoću SQL INSERT INTO naredbe i preddefiniranih parametara, uz pomoć pdo objekta, stvara zapis u bazi podataka
    function save()
    {
        $properties = array(
            ':title' => $this->_naziv_rada,
            ':text' => $this->_tekst_rada,
            ':link' => $this->_link_rada,
            ':identificationNumber' => $this->_oib_tvrtke,
        );

        $sql = <<<EOSQL
            INSERT INTO diplomski_radovi (naziv_rada, tekst_rada, link_rada, oib_tvrtke) 
            VALUES (:title, :text, :link, :identificationNumber);
        EOSQL;

        $query = $this->conn->prepare($sql);

        try {
            $query->execute($properties);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    //implementacija read metoda koja pomoću SQL SELECT FROM naredbe i pdo objekta, vrši dohvat iz baze podataka te dohvaćene zapise ispisuje u div element
    function read()
    {
        $sql = <<<EOSQL
        SELECT * FROM diplomski_radovi;
    EOSQL;

        $query = $this->conn->prepare($sql);

        try {
            $query->execute();
            $query->setFetchMode(PDO::FETCH_ASSOC);

            while ($row = $query->fetch()) {
                echo "<div>".$row["naziv_rada"].", ".$row["tekst_rada"].", ".$row["link_rada"].", ".$row["oib_tvrtke"]."</div>";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
