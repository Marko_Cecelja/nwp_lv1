<?php
include('./simple_html_dom.php');
include("DiplomskiRadovi.php");

//inicijalizacija objekta DiplomskiRadovi
$diplomskiRadovi = new DiplomskiRadovi();

//spajanje na server i pretvorba rezultata u dom uz pomoć simple_html_dom biblioteke
$url = 'https://stup.ferit.hr/zavrsni-radovi/page/2';
$fp  = fopen($url, 'r');
$read = fgetcsv($fp);

$read = file_get_html($url);

//obrada dom elemenata kako bi se iz html strukture izdvojili potrebni podaci za stvaranje i spremanje diplomskih radova u bazu
foreach ($read->find('article') as $article) {

    foreach ($article->find('ul.slides img') as $img) {
    }
    foreach ($article->find('h2.entry-title a') as $link) {
        $html = file_get_html($link->href);
        foreach ($html->find('.post-content') as $text) {
        }
    }

    $diplomskiRadovi->create($link->plaintext, $text->plaintext, $link->href, preg_replace('/[^0-9]/', '', $img->src));
    $diplomskiRadovi->save();
}

//ispis svih diplomskih radova nakon pohrane u bazu
$diplomskiRadovi->read();

fclose($fp);
