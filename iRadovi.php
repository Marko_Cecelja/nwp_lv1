<?php
//sučelje unutar kojeg su definirane create, save i read metode
interface iRadovi
{
    public function create($naziv_rada, $tekst_rada, $link_rada, $oib_tvtke);
    public function save();
    public function read();
}
