<?php

//konfiguracija baze koja se koristi za inicijalizaciju pdo objekta. Parametri su postavljeni za localhost
final class DBConfig
{
    const HOST = 'localhost';
    const DB_NAME = 'radovi';
    const USERNAME = 'root';
    const PASS = '';
}